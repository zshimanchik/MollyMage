import time


board_str = '''
☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
☼ #  #      # &       ☼
☼    ♥                ☼
☼#     #  #           ☼
☼      #   ♥         #☼
☼  # #      ##        ☼
☼    #            #   ☼
☼    &    #           ☼
☼##      # #          ☼
☼         ##   # # #  ☼
☼♥   ###    #         ☼
☼      ☺              ☼
☼   ##                ☼
☼&     #      &   ♥   ☼
☼  #    # #           ☼
☼       ##            ☼
☼ #       #    #      ☼
☼   #  #          #   ☼
☼       ♥     # &     ☼
☼  ##        # #      ☼
☼     #     #     #♥  ☼
☼        # #          ☼
☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
'''
board_str = ''.join(line.strip() for line in board_str.splitlines())

from player import Player
player = Player(process_count=0)

handling_time_list = []
result_size_list = []
for i in range(10):
    start = time.time()
    player.turn(board_str)
    handling_time = time.time() - start
    print(f'Loop: {i} time: {handling_time:.3f} Result size: {player.result_size}')
    handling_time_list.append(handling_time)
    result_size_list.append(player.result_size)

for i, (handling_time, result_size) in enumerate(zip(handling_time_list, result_size_list)):
    print(f'Loop: {i} time: {handling_time:.3f} Result size: {result_size}')
print(f'Avg time: {sum(handling_time_list) / len(handling_time_list):0.3f} Avg result: {sum(result_size_list) / len(result_size_list):.0f}')

# print(f'worker time list: {player.worker_time_list} ')
# print(f'avg worker: {sum(player.worker_time_list) / len(player.worker_time_list)}')

# print(player.queue_size_list)
