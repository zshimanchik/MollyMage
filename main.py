import logging

from client import DojoConnection
from player import Player
from colorama import init

init(autoreset=True)

logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.INFO)

RESTART_ON_EXCEPTION = False


def main():
    try:
        # Testbot
        url = 'https://dojorena.io/codenjoy-contest/board/player/dojorena1335?code=7236191556390423682'
        player = Player(process_count=7)
        ws = DojoConnection(url, player)
        ws.connect()
        ws.run_forever()
    except Exception as ex:
        print(ex)
    finally:
        try:
            print('terminating player')
            player.terminate()
        except Exception as ex:
            print(ex)
        try:
            ws.close()
        except Exception as ex:
            print(ex)


if __name__ == '__main__':
    main()
    while RESTART_ON_EXCEPTION:
        print("====restart====")
        main()
