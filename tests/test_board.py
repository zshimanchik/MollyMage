
import unittest

from engine.board import Board
from engine.point import Point


class TestGetMySnake(unittest.TestCase):

    def test_get_my_snake(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
            ☼☼ ®        ®                  ☼
            ☼☼                            ®☼
            ☼☼  ☼☼☼  ☼☼  ☼☼☼☼  ☼  ☼☼☼☼  ☼  ☼
            ☼#  ☼○   $☼  ☼○○☼  ☼  ☼○    ☼  ☼
            ☼☼  ☼  ☼● ☼  ☼    ☼☼  ☼  ☼ ○☼  ☼
            ☼☼  ☼◄═╗☼    ☼ ®●$  ® ☼  $ ☼☼  ☼
            ☼☼  ☼☼ ╚╕    ☼☼         ●  ®   ☼
            ☼☼        ☼      ☼ ☼      ☼    ☼
            ☼#®   ☼   ☼  ☼○   ○☼  ☼○   ○☼  ☼
            ☼☼  ☼☼☼  ☼☼  ☼☼☼  ☼☼  ☼☼☼  ☼☼  ☼
            ☼☼                             ☼
            ☼☼       ┌─>                   ☼
            ☼☼  ☼☼☼☼ │☼  ☼☼☼☼  ☼  ☼☼☼  ☼☼  ☼
            ☼#  ☼  ☼ ¤☼  ☼○    ☼  ☼$   ○☼  ☼
            ☼☼  ☼    ☼☼  ☼  ☼ ○☼  ☼  ☼● ☼  ☼
            ☼☼  ☼  ●     ☼  $ ☼☼  ☼○  ☼    ☼
            ☼☼  ☼☼         ●      ☼☼  ○    ☼
            ☼☼      ☼ ☼      ☼          ☼  ☼
            ☼#  ☼    ○☼  ☼○   ○☼    ☼  ○☼  ☼
            ☼☼  ☼☼☼  ☼☼  ☼☼☼  ☼☼  ☼☼☼  ☼☼  ☼
            ☼☼                             ☼
            ☼☼                             ☼
            ☼☼  ☼☼☼☼  ☼☼☼☼ ○☼☼☼☼  ☼☼☼☼  ☼  ☼
            ☼☼  ☼  ○●       ☼$      ☼○  ☼  ☼
            ☼#     ☼    ●○    ☼   ☼$  ☼    ☼
            ☼☼    ●$    ☼   ○☼     ●  ☼    ☼
            ☼☼  ☼   ○☼○      ○  ☼○ ○  ○ ☼  ☼
            ☼☼  ☼  ☼☼☼☼  ☼☼☼☼☼  ☼☼☼☼  ☼☼☼  ☼
            ☼☼ ®    ○                ®  ®  ☼
            ☼☼                             ☼
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
        '''
        board = Board(''.join(line.strip() for line in board_str.splitlines()))

        snake = board.get_my_snake
        print(snake)
        self.assertEqual(
            snake,
            [Point(x=5, y=6), Point(x=6, y=6), Point(x=7, y=6), Point(x=7, y=7), Point(x=8, y=7)]
        )

    # def test_get_my_snake_with_dead_enemy_inside(self):
    #     board_str = '''
    #         ☼☼☼☼☼☼
    #         ☼×┐╔►☼
    #         ☼●│║○☼
    #         ☼╘☺╝○☼
    #         ☼● ● ☼
    #         ☼☼☼☼☼☼
    #     '''
    #     board = Board(''.join(line.strip() for line in board_str.splitlines()))
    #
    #     snake = board.get_my_snake
    #     print(snake)
    #     self.assertEqual(
    #         snake,
    #         [Point(x=5, y=6), Point(x=6, y=6), Point(x=7, y=6), Point(x=7, y=7), Point(x=8, y=7)]
    #     )


class TestGetEnemySnake(unittest.TestCase):

    def test_get_snake_by_rage_head(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼☼☼
            ☼☼  ☼☼  ☼☼ 
            ☼  ┌───┐ ☼
            ☼  │┌─♣¤ ☼
            ☼☼☼││☼  ☼☼
            ☼ ☼└┘☼  ☼☼
            ☼   ☼☼  ☼☼
            ☼ ●     ☼☼
            ☼        ☼
            ☼☼☼☼☼☼☼☼☼☼
        '''
        board = Board(''.join(line.strip() for line in board_str.splitlines()))
        snake = board.get_enemy_snake(Point(6, 3))
        print(snake)
        self.assertEqual(
            snake,
            [Point(x=6, y=3), Point(x=5, y=3), Point(x=4, y=3), Point(x=4, y=4), Point(x=4, y=5), Point(x=3, y=5),
             Point(x=3, y=4), Point(x=3, y=3), Point(x=3, y=2), Point(x=4, y=2), Point(x=5, y=2), Point(x=6, y=2),
             Point(x=7, y=2), Point(x=7, y=3)]
        )

    def test_get_snake_by_head(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼☼☼
            ☼☼  ☼☼  ☼☼ 
            ☼  ┌───┐ ☼
            ☼  │┌─>¤ ☼
            ☼☼☼││☼  ☼☼
            ☼ ☼└┘☼  ☼☼
            ☼   ☼☼  ☼☼
            ☼ ●     ☼☼
            ☼        ☼
            ☼☼☼☼☼☼☼☼☼☼
        '''
        board = Board(''.join(line.strip() for line in board_str.splitlines()))
        snake = board.get_enemy_snake(Point(6, 3))
        print(snake)
        self.assertEqual(
            snake,
            [Point(x=6, y=3), Point(x=5, y=3), Point(x=4, y=3), Point(x=4, y=4), Point(x=4, y=5), Point(x=3, y=5),
             Point(x=3, y=4), Point(x=3, y=3), Point(x=3, y=2), Point(x=4, y=2), Point(x=5, y=2), Point(x=6, y=2),
             Point(x=7, y=2), Point(x=7, y=3)]
        )

    def test_get_snake_by_body(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼☼☼
            ☼☼  ☼☼  ☼☼ 
            ☼  ┌───┐ ☼
            ☼  │┌─♣¤ ☼
            ☼☼☼││☼  ☼☼
            ☼ ☼└┘☼  ☼☼
            ☼   ☼☼  ☼☼
            ☼ ●     ☼☼
            ☼        ☼
            ☼☼☼☼☼☼☼☼☼☼
        '''
        board = Board(''.join(line.strip() for line in board_str.splitlines()))
        snake = board.get_enemy_snake(Point(4, 3))
        print(snake)
        self.assertEqual(
            snake,
            [Point(x=6, y=3), Point(x=5, y=3), Point(x=4, y=3), Point(x=4, y=4), Point(x=4, y=5), Point(x=3, y=5),
             Point(x=3, y=4), Point(x=3, y=3), Point(x=3, y=2), Point(x=4, y=2), Point(x=5, y=2), Point(x=6, y=2),
             Point(x=7, y=2), Point(x=7, y=3)]
         )

    def test_get_snake_by_tail(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼☼☼
            ☼☼  ☼☼  ☼☼ 
            ☼  ┌───┐ ☼
            ☼  │┌─♣¤ ☼
            ☼☼☼││☼  ☼☼
            ☼ ☼└┘☼  ☼☼
            ☼   ☼☼  ☼☼
            ☼ ●     ☼☼
            ☼        ☼
            ☼☼☼☼☼☼☼☼☼☼
        '''
        board = Board(''.join(line.strip() for line in board_str.splitlines()))
        snake = board.get_enemy_snake(Point(7, 3))
        print(snake)
        self.assertEqual(
            snake,
            [Point(x=6, y=3), Point(x=5, y=3), Point(x=4, y=3), Point(x=4, y=4), Point(x=4, y=5), Point(x=3, y=5),
             Point(x=3, y=4), Point(x=3, y=3), Point(x=3, y=2), Point(x=4, y=2), Point(x=5, y=2), Point(x=6, y=2),
             Point(x=7, y=2), Point(x=7, y=3)]
         )

    def test_get_short_snake_by_head(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼☼☼
            ☼☼  ☼☼  ☼☼ 
            ☼        ☼
            ☼     ♣ö ☼
            ☼☼☼  ☼  ☼☼
            ☼ ☼  ☼  ☼☼
            ☼   ☼☼  ☼☼
            ☼ ●     ☼☼
            ☼        ☼
            ☼☼☼☼☼☼☼☼☼☼
        '''
        board = Board(''.join(line.strip() for line in board_str.splitlines()))
        snake = board.get_enemy_snake(Point(6, 3))
        print(snake)
        self.assertEqual(
            snake,
            [Point(x=6, y=3), Point(x=7, y=3)]
         )
