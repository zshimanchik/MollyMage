
import unittest

# from mock import Mock
from engine.actions import Action
from engine.board import Board
from engine.world import World, BoardInfo
from tests.uutils import strip_board_str


class TestWorldBoardRender(unittest.TestCase):
    # def setUp(self):
    #     self.converter = MacAddressPlusOneConverter()
    #     self.log = Mock()

    # def test_get_nb_positive(self):
    #     board = Board(
    #         '☼  '
    #         '╘► '
    #         '☼  '
    #     )
    #     new_board = World(board, Action.RIGHT).move_me()
    #     print(new_board)
    #     self.assertEqual(new_board._string,
    #         '☼  '
    #         ' ═▲'
    #         '☼  '
    #      )
    #
    #
    #     # result = self.converter.get_nb(self.log, 'AC:22:05:9E:1A:3F')
        # self.assertEqual(result, 'AC:22:05:9E:1A:40')

    def test_1(self):
        board_str =  '''
            ☼☼☼☼☼☼
            ☼    ☼
            ☼ ♥  ☼
            ☼ 1☺ ☼
            ☼ ## ☼
            ☼☼☼☼☼☼  
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())
        reward, new_board, new_board_info = World(Board(board_str), BoardInfo(), Action.RIGHT).calculate()
        # print(reward, new_board.to_string())
        self.assertIsNone(new_board)
        # expected_board_str = strip_board_str('''
        #     ☼☼☼☼☼☼☼☼
        #     ☼      ☼
        #     ☼ ☼    ☼
        #     ☼  ☼   ☼
        #     ☼  ☼   ☼
        #     ☼ ♥══╗╓☼
        #     ☼  ☼ ╚╝☼
        #     ☼☼☼☼☼☼☼☼
        # ''')
        # self.assertEqual(new_board._string, expected_board_str)


    def test_2(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
            ☼   #    ##      2  ☺ ☼
            ☼ ☼☼#☼☼☼☼♥☼  ☼☼ ☼☼##☼ ☼
            ☼ ☼☼  #   ☼☼ ☼☼ ☼☼ ☼☼ ☼
            ☼ #   ☼☼☼##☼ &      ☼ ☼
            ☼ ☼☼☼   ☼##  ☼☼☼☼ ☼   ☼
            ☼  ☼ &☼   ☼☼   #  ☼☼☼ ☼
            ☼     ☼☼☼# ☼☼#  ☼     ☼
            ☼ ☼☼☼          ☼☼☼# ☼ ☼
            ☼# ☼# ☼☼☼☼ ☼☼☼ # # ☼☼ ☼
            ☼      &  ##☼  #☼## ☼ ☼
            ☼ ☼☼☼☼ ☼☼ ☼   ☼☼☼ ☼ # ☼
            ☼   #  ☼☼#☼☼☼     ☼☼☼ ☼
            ☼#☼☼☼# # #    ☼☼☼   # ☼
            ☼& ☼  ☼☼ ☼☼☼   ☼  ☼☼☼ ☼
            ☼  # ☼☼# ##☼ ☼#     ☼ ☼
            ☼ ☼#     ☼   ☼☼☼ ☼☼#  ☼
            ☼ ☼♥ ☼☼  ☼☼#     ☼☼ ☼ ☼
            ☼ ☼☼ ☼☼   ☼ ☼☼☼☼ #  ☼ ☼
            ☼   #   ☼ #  # # ☼☼ ☼&☼
            ☼ ☼☼☼☼#☼☼☼ ☼☼☼☼ ☼☼  ☼ ☼
            ☼##    ♥#       ##  ♥ ☼
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())
        reward, new_board, new_board_info = World(Board(board_str), BoardInfo(), Action.LEFT).calculate()
        print(reward, new_board.to_string())
        self.assertIsNone(new_board)

    def test_3(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
            ☼   #    ##         ☺ ☼
            ☼ ☼☼#☼☼☼☼♥☼  ☼☼ ☼☼11☼ ☼
            ☼ ☼☼  #   ☼☼ ☼☼ ☼☼ ☼☼ ☼
            ☼ #   ☼☼☼##☼ &      ☼ ☼
            ☼ ☼☼☼   ☼##  ☼☼☼☼ ☼   ☼
            ☼  ☼ &☼   ☼☼   #  ☼☼☼ ☼
            ☼     ☼☼☼# ☼☼#  ☼     ☼
            ☼ ☼☼☼          ☼☼☼# ☼ ☼
            ☼# ☼# ☼☼☼☼ ☼☼☼ # # ☼☼ ☼
            ☼      &  ##☼  #☼## ☼ ☼
            ☼ ☼☼☼☼ ☼☼ ☼   ☼☼☼ ☼ # ☼
            ☼   #  ☼☼#☼☼☼     ☼☼☼ ☼
            ☼#☼☼☼# # #    ☼☼☼   # ☼
            ☼& ☼  ☼☼ ☼☼☼   ☼  ☼☼☼ ☼
            ☼  # ☼☼# ##☼ ☼#     ☼ ☼
            ☼ ☼#     ☼   ☼☼☼ ☼☼#  ☼
            ☼ ☼♥ ☼☼  ☼☼#     ☼☼ ☼ ☼
            ☼ ☼☼ ☼☼   ☼ ☼☼☼☼ #  ☼ ☼
            ☼   #   ☼ #  # # ☼☼ ☼&☼
            ☼ ☼☼☼☼#☼☼☼ ☼☼☼☼ ☼☼  ☼ ☼
            ☼##    ♥#       ##  ♥ ☼
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())
        reward, new_board, new_board_info = World(Board(board_str), BoardInfo(), Action.LEFT).calculate()
        # print(reward, new_board.to_string())
        self.assertIsNone(new_board)




    def test_draw_moved_snake(self):
        board_str = '''
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
            ☼☼                            ®☼
            ☼☼                             ☼
            ☼☼  ☼☼☼  ☼☼  ☼☼☼☼  ☼  ☼☼☼☼  ☼  ☼
            ☼#  ☼     ☼  ☼○○☼  ☼  ☼○   ®☼  ☼
            ☼☼  ☼  ☼  ☼  ☼    ☼☼  ☼  ☼ ○☼  ☼
            ☼☼  ☼   ☼    ☼  ●$    ☼  $ ☼☼  ☼
            ☼☼  ☼☼       ☼☼         ●      ☼
            ☼☼ ╘╗▲    ☼ ○    ☼ ☼   ○  ☼    ☼
            ☼#  ╚╝☼   ☼  ☼○®  ○☼  ☼○   ○☼  ☼
            ☼☼  ☼☼☼  ☼☼  ☼☼☼  ☼☼  ☼☼☼  ☼☼  ☼
            ☼☼                 ®           ☼
            ☼☼               ® ®           ☼
            ☼☼  ☼☼☼☼  ☼  ☼☼☼☼  ☼  ☼☼☼  ☼☼  ☼
            ☼# ®☼  ☼ ○☼  ☼○    ☼  ☼$   ○☼  ☼
            ☼☼  ☼    ☼☼  ☼  ☼ ○☼  ☼  ☼● ☼  ☼
            ☼☼  ☼○       ☼ ┌─♣☼☼  ☼○  ☼    ☼
            ☼☼  ☼☼         │      ☼☼  ○    ☼
            ☼☼      ☼ ☼ ┌─┐│ ☼          ☼  ☼
            ☼#  ☼     ☼ │☼└┘  ○☼    ☼  ○☼  ☼
            ☼☼  ☼☼☼  ☼☼ │☼☼☼  ☼☼  ☼☼☼  ☼☼  ☼
            ☼☼          │                  ☼
            ☼☼         ×┘   ®              ☼
            ☼☼  ☼☼☼☼  ☼☼☼☼  ☼☼☼☼  ☼☼☼☼  ☼  ☼
            ☼☼  ☼           ☼       ☼   ☼  ☼
            ☼#     ☼          ☼   ☼   ☼    ☼
            ☼☼          ☼    ☼     ●  ☼    ☼
            ☼☼  ☼    ☼          ☼       ☼  ☼
            ☼☼  ☼  ☼☼☼☼  ☼☼☼☼☼  ☼☼☼☼  ☼☼☼  ☼
            ☼☼                       ®     ☼
            ☼☼                ®            ☼
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
        '''
        board_str = ''.join(line.strip() for line in board_str.splitlines())
        board = Board(board_str)
        # board.print_board()

        new_board = World(board, BoardInfo(0, 0), Action.RIGHT).move_me(False)

        expected_board_str = '''
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼
            ☼☼                            ®☼
            ☼☼                             ☼
            ☼☼  ☼☼☼  ☼☼  ☼☼☼☼  ☼  ☼☼☼☼  ☼  ☼
            ☼#  ☼     ☼  ☼○○☼  ☼  ☼○   ®☼  ☼
            ☼☼  ☼  ☼  ☼  ☼    ☼☼  ☼  ☼ ○☼  ☼
            ☼☼  ☼   ☼    ☼  ●$    ☼  $ ☼☼  ☼
            ☼☼  ☼☼       ☼☼         ●      ☼
            ☼☼  ╓╔►   ☼ ○    ☼ ☼   ○  ☼    ☼
            ☼#  ╚╝☼   ☼  ☼○®  ○☼  ☼○   ○☼  ☼
            ☼☼  ☼☼☼  ☼☼  ☼☼☼  ☼☼  ☼☼☼  ☼☼  ☼
            ☼☼                 ®           ☼
            ☼☼               ® ®           ☼
            ☼☼  ☼☼☼☼  ☼  ☼☼☼☼  ☼  ☼☼☼  ☼☼  ☼
            ☼# ®☼  ☼ ○☼  ☼○    ☼  ☼$   ○☼  ☼
            ☼☼  ☼    ☼☼  ☼  ☼ ○☼  ☼  ☼● ☼  ☼
            ☼☼  ☼○       ☼ ┌─♣☼☼  ☼○  ☼    ☼
            ☼☼  ☼☼         │      ☼☼  ○    ☼
            ☼☼      ☼ ☼ ┌─┐│ ☼          ☼  ☼
            ☼#  ☼     ☼ │☼└┘  ○☼    ☼  ○☼  ☼
            ☼☼  ☼☼☼  ☼☼ │☼☼☼  ☼☼  ☼☼☼  ☼☼  ☼
            ☼☼          │                  ☼
            ☼☼         ×┘   ®              ☼
            ☼☼  ☼☼☼☼  ☼☼☼☼  ☼☼☼☼  ☼☼☼☼  ☼  ☼
            ☼☼  ☼           ☼       ☼   ☼  ☼
            ☼#     ☼          ☼   ☼   ☼    ☼
            ☼☼          ☼    ☼     ●  ☼    ☼
            ☼☼  ☼    ☼          ☼       ☼  ☼
            ☼☼  ☼  ☼☼☼☼  ☼☼☼☼☼  ☼☼☼☼  ☼☼☼  ☼
            ☼☼                       ®     ☼
            ☼☼                ®            ☼
            ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼'''
        expected_board_str = ''.join(line.strip() for line in expected_board_str.splitlines())
        expected_board = Board(expected_board_str)
        # new_board.print_board()
        self.assertEqual(new_board._string, expected_board._string)

    def test_snake_fight_neck_me_evil_enemy_not_evil(self):
        board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼ ¤☼   ☼
            ☼ │☼  ╓☼
            ☼ ˅♥═╗║☼
            ☼  ☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        reward, new_board, new_board_info = World(Board(board_str), BoardInfo(0, 2), Action.LEFT).calculate()

        expected_board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼  ☼   ☼
            ☼  ☼   ☼
            ☼ ♥══╗╓☼
            ☼  ☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        self.assertEqual(new_board._string, expected_board_str)

    def test_snake_fight_neck_me_evil_enemy_evil(self):
        board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼ ¤☼   ☼
            ☼ │☼  ╓☼
            ☼ ♣♥═╗║☼
            ☼  ☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        reward, new_board, new_board_info = World(Board(board_str), BoardInfo(0, 2), Action.LEFT).calculate()

        expected_board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼  ☼   ☼
            ☼  ☼   ☼
            ☼ ♥══╕ ☼
            ☼  ☼   ☼
            ☼☼☼☼☼☼☼☼
        ''')
        self.assertEqual(new_board._string, expected_board_str)


    def test_snake_fight_neck_me_not_evil_enemy_not_evil(self):
        board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼ ¤☼   ☼
            ☼ │☼  ╓☼
            ☼ ˅◄═╗║☼
            ☼  ☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        reward, new_board, new_board_info = World(Board(board_str), BoardInfo(0, 0), Action.LEFT).calculate()

        expected_board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼  ☼   ☼
            ☼  ☼   ☼
            ☼ ◄══╕ ☼
            ☼  ☼   ☼
            ☼☼☼☼☼☼☼☼
        ''')
        self.assertEqual(new_board._string, expected_board_str)

    def test_snake_fight_body_me_evil_enemy_not_evil(self):
        board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼ ¤☼   ☼
            ☼ │☼  ╓☼
            ☼ │♥═╗║☼
            ☼<┘☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        reward, new_board, new_board_info = World(Board(board_str), BoardInfo(0, 2), Action.LEFT).calculate()
        # new_board.print_board()

        expected_board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼  ☼   ☼
            ☼  ☼   ☼
            ☼ ♥══╗╓☼
            ☼<ö☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        self.assertEqual(new_board._string, expected_board_str)

    def test_snake_fight_body_me_evil_enemy_not_evil_depth_2(self):
        board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼ ¤☼   ☼
            ☼ │☼  ╓☼
            ☼ │♥═╗║☼
            ☼<┘☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        reward, new_board, new_board_info = World(Board(board_str), BoardInfo(0, 2), Action.LEFT, 2).calculate()
        # new_board.print_board()

        expected_board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼  ☼   ☼
            ☼  ☼   ☼
            ☼ ♥══╗╓☼
            ☼<ö☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        self.assertEqual(new_board._string, expected_board_str)


    def test_snake_fight_body_me_evil_enemy_evil(self):
        board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼ ¤☼   ☼
            ☼ │☼  ╓☼
            ☼ │♥═╗║☼
            ☼♣┘☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        reward, new_board, new_board_info = World(Board(board_str), BoardInfo(0, 2), Action.LEFT).calculate()
        # new_board.print_board()

        expected_board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼  ☼   ☼
            ☼  ☼   ☼
            ☼ ♥══╗╓☼
            ☼♣ö☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        self.assertEqual(new_board._string, expected_board_str)

    def test_snake_fight_body_me_evil_enemy_evil_depth_2(self):
        board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼ ¤☼   ☼
            ☼ │☼  ╓☼
            ☼ │♥═╗║☼
            ☼♣┘☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        reward, new_board, new_board_info = World(Board(board_str), BoardInfo(0, 2), Action.LEFT, 2).calculate()
        # new_board.print_board()

        expected_board_str = strip_board_str('''
            ☼☼☼☼☼☼☼☼
            ☼      ☼
            ☼ ☼    ☼
            ☼  ☼   ☼
            ☼  ☼   ☼
            ☼ ♥══╗╓☼
            ☼♣ö☼ ╚╝☼
            ☼☼☼☼☼☼☼☼
        ''')
        self.assertEqual(new_board._string, expected_board_str)
