import colorama
from ws4py.client.threadedclient import WebSocketClient
import time
import datetime


class DojoConnection(WebSocketClient):
    def __init__(self, url, player):
        path = url.replace("http", "ws")
        path = path.replace("board/player/", "ws?user=")
        path = path.replace("?code=", "&code=")

        super(DojoConnection, self).__init__(path)
        self.player = player
        self.prev_time = time.time()

    def received_message(self, message):
        print(f'Received ws message at: {datetime.datetime.now().isoformat()}')
        board = str(message).lstrip("board=")
        time_waited = time.time() - self.prev_time
        self.prev_time = time.time()
        response = self.player.turn(board)
        time_thinking = time.time() - self.prev_time
        self.prev_time = time.time()
        self.send(response)
        time_sending = time.time() - self.prev_time
        self.prev_time = time.time()
        print(f'Responded to ws message at: {datetime.datetime.now().isoformat()} message: {response!r}')
        print(f'{colorama.Fore.BLUE}'
              f'Wait: {time_waited:.3f} '
              f'Think: {time_thinking:.3f} '
              f'Send: {time_sending:.3f}'
              f'{colorama.Style.RESET_ALL}')

