from math import sqrt

from cached_property import cached_property

from engine import element
from engine.point import Point, Direction


class Board:
    """ Class describes the Board field for Bomberman game."""
    # Fucking shit can overlap and can't be described with just a string.
    # Overlapping:
    # ghost and bomb
    # bomb and BOOM
    # If time for bomb didn't change after tick then it's remote control bomb

    def __init__(self, board_string):
        self._string = board_string.replace('\n', '')
        self._len = len(self._string)  # the length of the string
        self._size = int(sqrt(self._len))  # size of the board

    def need_to_restart(self):
        return element.DEAD_HERO in self._string

    def get_point_by_shift(self, shift):
        return Point(shift % self._size, int(shift / self._size))

    @cached_property
    def is_game_active(self):
        return element.HERO in self._string or element.POTION_HERO in self._string

    @cached_property
    def me(self):
        hero_pos = self.find_element_pos(element.HERO)
        if not hero_pos:
            hero_pos = self.find_element_pos(element.POTION_HERO)
        return hero_pos

    def get_shift_by_point(self, point):
        return point.y * self._size + point.x

    def _strpos2pt(self, strpos):
        return Point(*self._strpos2xy(strpos))

    def _strpos2xy(self, strpos):
        return (strpos % self._size, strpos // self._size)

    def _xy2strpos(self, x, y):
        return self._size * y + x

    def p2strpos(self, point):
        return self._size * point.y + point.x

    def print_board(self):
        print(self._line_by_line())

    def _line_by_line(self):
        return '\n'.join([self._string[i:i + self._size]
                          for i in range(0, self._len, self._size)])

    def to_string(self):
        return ("Board:\n{brd}".format(brd=self._line_by_line()))

    def get_element_at(self, point):
        """ Return an Element object at coordinates x,y."""
        return self._string[self._xy2strpos(point.x, point.y)]

    def has_element_at(self, point, element_object):
        if point.is_out_of_board(self._size):
            return False
        return element_object == self.get_element_at(point)

    def find_element_i(self, element):
        try:
            return self._string.index(element)
        except ValueError:
            return None

    def find_element_pos(self, element):
        i = self.find_element_i(element)
        if i is not None:
            return self.get_point_by_shift(i)
        else:
            return None





if __name__ == '__main__':
    raise RuntimeError("This module is not designed to be ran from CLI")
