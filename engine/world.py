import random
from dataclasses import dataclass

from engine import element
from engine.board import Board
from engine.point import Point, Direction


@dataclass(frozen=True)
class BoardInfo:
    tick: int = 0
    evil_countdown: int = 0


class World:
    BLAST_SIZE = 3
    def __init__(self, board, board_info, action, depth=1):
        """
        :param board:
        :param board_info:
        :param action:
        :param depth: depth from task - how much ticks away in future this board from actual situation
        """
        assert depth > 0

        self.board = board
        self.action = action
        self.depth = depth
        self.board_info = board_info

    def calculate(self):
        """
        :return: Reward, New Board or None if game over, New Board Info
        """
        if not self.board.is_game_active:
            return 0, None, self.board_info

        # Change the world
        new_board = list(self.board._string)
        new_board_info = BoardInfo(self.board_info.tick + 1, max(0, self.board_info.evil_countdown - 1))
        reward = 0

        # clean the "boom"s and dead people
        for i, el in enumerate(new_board):
            if el in (element.BOOM, element.DEAD_GHOST, element.OTHER_DEAD_HERO, element.ENEMY_DEAD_HERO):
                new_board[i] = element.NONE

        # move me
        next_pos = self.board.me + Direction.from_action(self.action).to_point()
        next_pos_el = self.board.get_element_at(next_pos)
        if next_pos_el in element.POTIONS:
            new_board = self.move_me(new_board, next_pos)
            reward += 10
        elif next_pos_el == element.OPENING_TREASURE_BOX:
            new_board = self.move_me(new_board, next_pos)
            reward += 5  # probably something good will be out of the box.
        elif next_pos_el == element.GHOST:
            return -300, None, new_board_info
        elif next_pos_el in element.WALKABLE:
            new_board = self.move_me(new_board, next_pos)
            reward += 1  # survived and moved. good enough for me
        else:  # Not walkable. we stay.
            reward -= 1  # don't do stupid things pls


        # Blow the bombs
        for i, el in enumerate(new_board):
            if el == element.POTION_TIMER_1:
                self.blow_bomb(new_board, i, self.BLAST_SIZE)

        # Decrease bomb timer
        for i, el in enumerate(new_board):
            if el == element.POTION_TIMER_2:
                new_board[i] = element.POTION_TIMER_1
            if el == element.POTION_TIMER_3:
                new_board[i] = element.POTION_TIMER_2
            if el == element.POTION_TIMER_4:
                new_board[i] = element.POTION_TIMER_3
            if el == element.POTION_TIMER_5:
                new_board[i] = element.POTION_TIMER_4

        # Estimate the outcome
        if element.DEAD_HERO in new_board:
            return -300, None, new_board_info  # dying is bad
        if element.OTHER_DEAD_HERO in new_board:
            reward += 10  # killing others is good
        if element.ENEMY_DEAD_HERO in new_board:
            reward += 20  # killing others is good




        return reward, Board(''.join(new_board)), new_board_info


    def move_me(self, new_board, next_pos):
        # print(f'Moving {self.action}')
        new_board[self.board.p2strpos(next_pos)] = element.HERO
        new_board[self.board.p2strpos(self.board.me)] = element.NONE
        return new_board

    def blow_bomb(self, new_board, i, length):
        new_board[i] = element.BOOM
        directions = [1, -1, self.board._size, -self.board._size]

        for direction in directions:
            for j in range(1, length+1):
                new_i = i + (direction * j)
                blow_elem = new_board[new_i]
                if blow_elem in element.WALKABLE:
                    new_board[new_i] = element.NONE
                elif blow_elem in (element.HERO, element.POTION_HERO):
                    new_board[new_i] = element.DEAD_HERO
                    break
                elif blow_elem == element.OTHER_HERO:
                    new_board[new_i] = element.OTHER_DEAD_HERO
                    break
                elif blow_elem == element.ENEMY_HERO:
                    new_board[new_i] = element.ENEMY_DEAD_HERO
                    break
                elif blow_elem in (element.ENEMY_POTION_HERO, element.OTHER_POTION_HERO) or blow_elem in element.POTION_TIMERS:
                    pass  # they don't explode another bombs
                    # self.blow_bomb(new_board, new_i, length)
                    # break
                elif blow_elem == element.TREASURE_BOX:
                    new_board[new_i] = element.OPENING_TREASURE_BOX
                    break
                elif blow_elem == element.GHOST:
                    new_board[new_i] = element.DEAD_GHOST
                elif blow_elem == element.WALL:
                    break # wall is left
                else:
                    raise Exception(f"Unexpected blow_elem {blow_elem}")





    @staticmethod
    def choose_best_possible_scenario(possible_scenarios):

        max_reward = max(task.reward for task in possible_scenarios)
        best_task_list = [task for task in possible_scenarios if task.reward == max_reward]
        print(f'Best_task_list len: {len(best_task_list)}')

        # for best in best_task_list:
        #     print(best)

        best_task = random.choice(best_task_list)
        return best_task





