



import logging

from client import DojoConnection
from player import Player
from colorama import init

init(autoreset=True)

logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.INFO)

RESTART_ON_EXCEPTION = False


def main():
    try:
        # Zakhar Shymanchyk
        # url = 'http://codebattle-pro-2020s1.westeurope.cloudapp.azure.com/codenjoy-contest/board/player/z4lxi9dcxrzw7n2wwjk1?code=8853386976345746662&gameName=snakebattle'
        url = 'https://dojorena.io/codenjoy-contest/board/player/dojorena1335?code=7236191556390423682'
        player = Player(process_count=1)
        ws = DojoConnection(url, player)
        ws.connect()
        ws.run_forever()
    except Exception as ex:
        print(ex)
    finally:
        try:
            ws.close()
        except Exception as ex:
            print(ex)


if __name__ == '__main__':
    main()
    while RESTART_ON_EXCEPTION:
        main()
