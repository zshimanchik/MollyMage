import collections
import math
import resource
import sys
from functools import partial
from threading import Thread

from PyQt5.QtWidgets import QApplication


from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import QRect, Qt, QTimer
from PyQt5.QtGui import QPen, QBrush, QColor

from client import DojoConnection




class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.central_widget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.central_widget)

        self.setWindowTitle("shitty control")
        self.resize(300, 300)
        self.pressed_keys_queue = collections.deque()
        self.board = ''

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.repaint)
        self.timer.start(200)

    def paintEvent(self, event):
        if not self.board:
            return
        n = int(math.sqrt(len(self.board)))
        board = '\n'.join(self.board[i:i + n] for i in range(0, len(self.board), n))
        # print(board)


        qp = QtGui.QPainter()
        qp.begin(self)
        # qp.drawText(QRect(0, 0, 100, 100), Qt.AlignLeft, "Qt\nhello")
        # if self.board:
        QtGui.QFont.TypeWriter
        qp.drawText(QRect(0, 0, self.width(), self.height()), Qt.AlignLeft, board)
        qp.end()

    def keyPressEvent(self, event: QtGui.QKeyEvent):
        # print('press', event.key())
        action = None
        if event.key() == Qt.Key_Up:
            action = 'up'
        elif event.key() == Qt.Key_Down:
            action = 'down'
        elif event.key() == Qt.Key_Left:
            action = 'left'
        elif event.key() == Qt.Key_Right:
            action = 'right'
        elif event.key() == Qt.Key_Space:
            action = 'ACT'

        if action:
            self.pressed_keys_queue.append(action)

        print(self.pressed_keys_queue)
        # print(Qt.Key_Up in self.pressed_keys)

    def keyReleaseEvent(self, event: QtGui.QKeyEvent):
        # print('release', event.key())
        pass
        # if event.key() in self.pressed_keys:
        #     self.pressed_keys.remove(event.key())

    def turn(self, board):
        self.board = board
        if self.pressed_keys_queue:
            action = self.pressed_keys_queue.popleft()
        else:
            action = 'STOP'

        print('action: ', action)
        return action


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mySW = MainWindow()

    url = "https://dojorena.io/codenjoy-contest/board/player/dojorena1335?code=7236191556390423682"
    ws = DojoConnection(url, mySW)
    ws.connect()

    t = Thread(target=ws.run_forever)
    t.start()

    mySW.show()
    sys.exit(app.exec_())
